#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>

#include <memory>
#include <string>

#include <cmw-rda3/client/service/ClientService.h>

enum class Mode
{
    OFF,
    COOL,
    WARM
};

class Client : public QObject
{
    Q_OBJECT

public:
    Client();

    void start(const std::string& frontEndName);

    void showMessage(const std::string& message);
    void updateModule(const std::string& id, float temperature, Mode mode);

signals:
    void showMessageSignal(QString message);
    void updateModuleSignal(QString id, float temperature, int mode);

private:
    std::unique_ptr<cmw::rda3::client::ClientService> client;
};

#endif // CLIENT_H
