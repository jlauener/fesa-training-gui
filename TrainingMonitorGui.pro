#-------------------------------------------------
#
# Project created by QtCreator 2019-01-31T10:21:56
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TrainingMonitorGui
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11
QMAKE_CXXFLAGS += -std=c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    client.cpp

HEADERS += \
        mainwindow.h \
    client.h

FORMS += \
        mainwindow.ui

INCLUDEPATH += /acc/local/L867/cmw/cmw-fwk/1.0.0/include
LIBS += -L/acc/local/L867/cmw/cmw-fwk/1.0.0/lib/ -lcmw-rda3 -lcmw-directory-client -lcmw-data -lcmw-log -lcmw-util -lzmq -lboost_1_69_0_filesystem -lboost_1_69_0_thread -lboost_1_69_0_chrono -lboost_1_69_0_atomic -lrt

