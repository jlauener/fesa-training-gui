
#include "client.h"

#include <cmw-rda3/client/service/ClientServiceBuilder.h>

using namespace cmw::rda3::client;
using namespace cmw::rda3::common;
using namespace cmw::data;

namespace
{

class MyListener : public cmw::rda3::client::NotificationListener
{
public:
    MyListener(Client& client) :
        client(client)
    {
    }

    void dataReceived(const Subscription & subscription, std::auto_ptr<AcquiredData> acqData, UpdateType updateType) override
    {
        try
        {
            const Data & data = acqData->getData();

            std::cout<<data.toString()<<std::endl;

            size_t moduleCount;
            const char** names = data.getArrayString("moduleId", moduleCount);
            const float* temperatures = data.getArrayFloat("moduleTemperature", moduleCount);
            const int32_t* modes = data.getArrayInt("moduleMode", moduleCount);

            for (size_t i = 0; i < moduleCount; i++)
            {
                client.updateModule(names[i], temperatures[i], static_cast<Mode>(modes[i]));
            }

            client.showMessage("Connected");
        }
        catch (const std::exception& ex)
        {
            client.showMessage(ex.what());
        }
    }

    void errorReceived(const Subscription & subscription, std::auto_ptr<RdaException> exception, UpdateType updateType) override
    {
        client.showMessage(exception->what());
    }

private:
    Client& client;
};

}

Client::Client()
{
    client = cmw::rda3::client::ClientServiceBuilder::newInstance()->build();
}

void Client::start(const std::string& frontEndName)
{
    std::tr1::shared_ptr<MyListener> listener(new MyListener(*this));
    client->getAccessPoint("TrainingMonitor." + frontEndName, "AllModules", "TrainingMonitor_DU." + frontEndName).subscribe(listener);
}

void Client::showMessage(const std::string& message)
{
    emit showMessageSignal(message.c_str());
}

void Client::updateModule(const std::string& message, float temperature, Mode mode)
{
    emit updateModuleSignal(message.c_str(), temperature, static_cast<int>(mode));
}
