#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QGroupBox>
#include <QLabel>
#include <QMainWindow>

#include "client.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void showMessage(QString message);
    void updateModule(QString id, float temperature, int mode);

private:
    QGroupBox* createModuleBox(QString name);
    QLabel* createModuleLabel(QString name);

    Ui::MainWindow *ui;
    Client client;
};

#endif // MAINWINDOW_H
